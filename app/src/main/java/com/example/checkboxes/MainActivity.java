package com.example.checkboxes;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CheckBox[] checkBoxes;
    private Button showToastButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkBoxes = new CheckBox[] {
                findViewById(R.id.checkbox1),
                findViewById(R.id.checkbox2),
                findViewById(R.id.checkbox3),
                findViewById(R.id.checkbox4),
                findViewById(R.id.checkbox5)
        };

        showToastButton = findViewById(R.id.showToastButton);
        showToastButton.setOnClickListener(v -> showSelectedCheckboxes());
    }

    private void showSelectedCheckboxes() {
        List<String> selectedCheckboxes = new ArrayList<>();

        for (CheckBox checkBox : checkBoxes) {
            if (checkBox.isChecked()) {
                selectedCheckboxes.add(checkBox.getText().toString());
            }
        }

        if (!selectedCheckboxes.isEmpty()) {
            String message;
            if (selectedCheckboxes.size() == 1) {
                message = "Selected checkbox: " + selectedCheckboxes.get(0);
            } else {
                message = "Selected checkboxes: " + String.join(", ", selectedCheckboxes);
            }
            showToast(message);
        } else {
            showToast("No checkboxes selected.");
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}